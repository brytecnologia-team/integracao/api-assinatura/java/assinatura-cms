package br.com.bry.framework.sample.util;

import br.com.bry.framework.sample.config.SignatureConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

public class FileUtil {
	
	public static void writeContentToFile(String filePath, String fileName, String content) throws IOException {

		SimpleDateFormat formatter = new SimpleDateFormat("_yyyy-MM-dd hh.mm.ss.SSS");

		String signatureFullName = filePath + fileName + formatter.format(new Date(System.currentTimeMillis())) + ".p7s";
		
		FileOutputStream in = new FileOutputStream(new File(signatureFullName));
		in.write(Base64.getDecoder().decode(content));
		in.close();

		System.out.println("Successful signature generation: " + SignatureConfig.OUTPUT_RESOURCE_FOLDER + signatureFullName);
		
	}
	
}
